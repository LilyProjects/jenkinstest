//
//  AppDelegate.h
//  Test
//
//  Created by Lily on 2018/10/23.
//  Copyright © 2018年 Lily. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

